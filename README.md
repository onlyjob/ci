## Notice

This CI setup is written specifically for Onlyjob's _gitlab-runner_ which
is hosted on private infrastructure and not affiliated with [Salsa CI
Team](https://salsa.debian.org/salsa-ci-team/pipeline).

Onlyjob's runner uses the Debian's official _packaged_ DFSG compliant
[gitlab-runner][].

Please ask if you with to use this setup and runner for your project.

[gitlab-runner]: https://tracker.debian.org/pkg/gitlab-ci-multi-runner

## Usage

See [example .gitlab-ci.yml](example.gitlab-ci.yml).

To avoid shipping "debian/.gitlab-ci.yml" in the package one can add the
following to "debian/clean" file:

~~~~
## Debian CI:
debian/.gitlab-ci.yml
~~~~
