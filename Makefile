#!/usr/bin/make -f
# -*- makefile -*-

.PHONY: yamllint

default: check

check: yamllint

help:
	@printf "Valid targets are: yamllint\n"

YMLs := $(shell find -xdev -type f -iname '*.yml')
yamllint:
	## YAML lint:
	@yamllint --version
	yamllint -c .yamllint $(YMLs)
